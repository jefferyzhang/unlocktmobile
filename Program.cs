﻿using androidmonkey;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UnlockTMobile
{
    class Program
    {
        static void Usage()
        {
            Console.Write(@"unlock tMobile android phone:
    -utilpath=adb.exe fullpath
    -label=x []
    -serial=device serial []
    -enable=true/false
    -ssid=wifi ssid
    -password=wifi psw
all dependance:
    fdbox721 must install, it is Open wifi.it is must version>=16.06.28.17
    if not need wifi, not need fdbox721.apk. 
return:
    unlock=true
    errorinfo=***(android screen info)
");

        }

        static int Main(string[] args)
        {
            System.Configuration.Install.InstallContext _param = new System.Configuration.Install.InstallContext(null, args);
            String sADB = "";
            if(_param.Parameters.ContainsKey("utilpath"))
            {
                sADB = System.Environment.ExpandEnvironmentVariables(_param.Parameters["utilpath"]);
            }
            if(!System.IO.File.Exists(sADB))
            {
                adbHelper.LogIt("adb not found.");
                Usage();
                return 1;
            }
            if(_param.Parameters.ContainsKey("label"))
            {
                adbHelper._label = Convert.ToInt32(_param.Parameters["label"]);
            }

            string sID = "";
            if(_param.Parameters.ContainsKey("serial"))
            {
                sID = _param.Parameters["serial"];
                sID = " -s " + sID;
            }
            

            adbHelper adb = new adbHelper(sADB);
            String sParam = String.Format(" {0} shell pm list packages", sID);
            adb.runAdb(sParam);
            String packages = adb.getAdbOutput();
            string[] ss = packages.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            Boolean bSupported = false;
            foreach (string item in ss)
            {
                if(item.IndexOf("com.tmobile.simlock")>0)
                {
                    bSupported = true;
                    break;
                }
            }
            if (!bSupported) return 11;

            String sFileName = Path.GetTempFileName();
            File.WriteAllBytes(sFileName, UnlockTMobile.Properties.Resources.UnlockTMobile);

            sParam = String.Format(" {0} push \"{1}\" /data/local/tmp/UnlockTMobile.jar", sID, sFileName);
            adb.runAdb(sParam);
            try
            {
                File.Delete(sFileName);
            }
            catch (Exception)
            {
            }

            //J:\Works\Eclipse\UnlockTMobile>adb shell am broadcast -a fdSyncReceiver.set_wifi -n com.futuredial.fdbox721/.fdSyncReceiver --es  password Fdial4082458880 --es ssid ASUS-AC --es type 3
            StringBuilder sEsparm = new StringBuilder();
            if (_param.Parameters.ContainsKey("enable")) sEsparm.Append(" --es enable ").Append(_param.Parameters["enable"]);
            if (_param.Parameters.ContainsKey("ssid")) sEsparm.Append(" --es ssid ").Append(_param.Parameters["ssid"]);
            if (_param.Parameters.ContainsKey("password")) sEsparm.Append(" --es password ").Append(_param.Parameters["password"]);
            if (_param.Parameters.ContainsKey("type")) sEsparm.Append(" --es type ").Append(_param.Parameters["type"]);
            sParam = String.Format(" {0} shell am broadcast -a fdSyncReceiver.set_wifi -n com.futuredial.fdbox721/.fdSyncReceiver {1}", sID, sEsparm.ToString());
            adb.runAdb(sParam);
            Thread.Sleep(3000);

            Boolean bstatus = false;
            String errorinfo = "";
            int RETRYCOUNT = 3;
            for (int i = 0; i < RETRYCOUNT; i++)
            {
                sParam = String.Format(" {0} shell uiautomator runtest /data/local/tmp/UnlockTMobile.jar -c com.fd.unlocktmobile.UnlockTMobile", sID);
                adb.runAdb(sParam, '\r', 300 * 1000);

                String s = adb.getAdbOutput();
                ss = s.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                foreach(string item in ss)
                {
                    if(item.StartsWith("errorinfo=",StringComparison.CurrentCultureIgnoreCase))
                    {
                        errorinfo = item.Substring("errorinfo=".Length);
                        break;
                    }
                    else if (item.StartsWith("status=", StringComparison.CurrentCultureIgnoreCase))
                    {
                        bstatus = true;
                        break;
                    }
                }
                if (bstatus) break;
                if (errorinfo.IndexOf("need retry") < 0) break; 
            }
            if (bstatus) Console.WriteLine("unlock=true");
            else Console.WriteLine("errorinfo=" + errorinfo);
            return 0;
        }

    }
}
